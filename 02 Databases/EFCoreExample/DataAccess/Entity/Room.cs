﻿namespace EFCoreExample.DataAccess.Entity;

using System.Collections.Generic;

public class Room
{
    public int Id { get; set; }
    
    public string RoomName { get; set; }
    
    public string Description { get; set; }
    
    public ICollection<Booking> Bookings { get; set; }
}
