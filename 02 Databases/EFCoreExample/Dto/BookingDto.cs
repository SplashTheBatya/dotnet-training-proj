﻿using EFCoreExample.DataAccess.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace EFCoreExample.Dto
{	
	
	public class BookingDto
	{
		public string Username { get; set; }
		public DateTime FromUtc { get; set; }
		public DateTime ToUtc { get; set; }
		public string Comment { get; set; }
		
		public int[] RoomIds { get; set; }
		public ICollection<Room> Rooms { get; set; }
		
		
		public Booking ToBooking(int userId)
		{	
			
			return new Booking
			{
				UserId = userId,
				FromUtc = DateTime.SpecifyKind(FromUtc, DateTimeKind.Utc),
				ToUtc = DateTime.SpecifyKind(ToUtc, DateTimeKind.Utc),
				Comment = Comment
			};
		}

		public static BookingDto FromBooking(Booking booking)
		{
			return new BookingDto
			{
				Comment = booking.Comment,
				FromUtc = booking.FromUtc,
				ToUtc = booking.ToUtc,
				Username = booking.User.UserName,
				Rooms = booking.Rooms
			};
		}
	}
}
