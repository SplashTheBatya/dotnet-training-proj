﻿using EFCoreExample.DataAccess;
using EFCoreExample.DataAccess.Entity;
using EFCoreExample.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace EFCoreExample.Controllers
{
	[Route("booking")]
	public class BookingController : Controller
	{
		private readonly BookingContext _bookingContext;

		public BookingController(BookingContext bookingContext)
		{
			_bookingContext = bookingContext;
		}

		public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
		{
			var bookings = await _bookingContext.Bookings
				.Include(b => b.User)
				.Include(r => r.Rooms)
				.ToArrayAsync(cancellationToken);
			return Ok(bookings.Select(b => BookingDto.FromBooking(b)));
		}

		[HttpPost]
		public async Task<ActionResult<BookingDto>> CreateBooking([FromForm] BookingDto bookingDto)
		{
			if (string.IsNullOrEmpty(bookingDto.Username))
				return BadRequest("Username cannot be empty");
			if (bookingDto.FromUtc == default)
				return BadRequest("FromUtc cannot be empty");
			if (bookingDto.ToUtc == default)
				return BadRequest("ToUtc cannot be empty");
			if (bookingDto.RoomIds.Length == 0)
				return BadRequest("RoomIds cannot be empty");
			

			User user = await _bookingContext
				.Users
				.FirstOrDefaultAsync(u => u.UserName == bookingDto.Username);
			if (user is null)
				return BadRequest($"User with name '{bookingDto.Username}' cannot be found");

			Room[] bookedRooms = _bookingContext.Rooms.Where(r => bookingDto.RoomIds
				.Contains(r.Id))
				.ToArray();
			if (bookedRooms.Length == 0)
				return BadRequest("There is no rooms with this Ids");
			
			Booking newBooking = bookingDto
				.ToBooking(userId: user.Id);
			newBooking.Rooms = bookedRooms;
			foreach (int RoomId in bookedRooms.Select(r => r.Id))
			{
				var alreadyCreatedBooking = await _bookingContext
					.Bookings
					.FirstOrDefaultAsync(b =>
						b.FromUtc <= newBooking.FromUtc
						&& b.ToUtc >= newBooking.FromUtc
						|| b.FromUtc <= newBooking.ToUtc
						&& b.ToUtc >= newBooking.ToUtc
						|| b.FromUtc == newBooking.FromUtc
						&& b.ToUtc == newBooking.ToUtc
						&& b.Rooms.Select(r => r.Id).Contains(RoomId));
				if (alreadyCreatedBooking is not null)
					return Conflict("Booking for this time has already been created");
			}
			if (newBooking.FromUtc < DateTime.UtcNow)
				return BadRequest("Cannot have from date earlier than now");
			if (newBooking.ToUtc - newBooking.FromUtc <= TimeSpan.FromMinutes(30))
				return BadRequest("Booking period should be at lease 30 minutes long");

			_bookingContext.Add(newBooking);
			await _bookingContext.SaveChangesAsync();
			return Ok(BookingDto.FromBooking(newBooking));
		}
		
		[HttpDelete]
		public async Task<ActionResult<BookingDto>> DeleteBooking([FromForm] int id)
		{
			Booking deletedBooking = _bookingContext.Find<Booking>(id);
			if (deletedBooking is null)
				return BadRequest($"There is no booking with id = {id}");
			_bookingContext.Remove(deletedBooking);
			await _bookingContext.SaveChangesAsync();
			return StatusCode(StatusCodes.Status204NoContent, "Booking deleted successfully");
		}
	}
}
