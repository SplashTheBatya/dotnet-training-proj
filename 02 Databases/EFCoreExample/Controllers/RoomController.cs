﻿using EFCoreExample.DataAccess;
using EFCoreExample.DataAccess.Entity;
using EFCoreExample.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace EFCoreExample.Controllers
{
    [Route("room")]
    public class RoomController : Controller
    {
        private readonly BookingContext _bookingContext;
        
        public RoomController(BookingContext bookingContext)
        {
            _bookingContext = bookingContext;
        }
        
        [Route("get_free")]
        public async Task<ActionResult<string[]>> GetAllFreeRoomName([FromForm] DateTime fromUtc, DateTime toUtc)
        {
            var exceptedRooms = _bookingContext.Bookings.AsEnumerable()
                .Where(b => b.FromUtc == fromUtc && b.ToUtc == toUtc || b.FromUtc <= fromUtc && b.ToUtc >= fromUtc || b.FromUtc <= toUtc && b.ToUtc >= toUtc)
                .SelectMany(r => r.Rooms)
                .Distinct().ToList();

            if (!exceptedRooms.Any())
                return Ok("There are no free rooms at this time");
                
            
            
            return await _bookingContext.Rooms.Where(i => !exceptedRooms.Contains(i))
                .Select(u => u.RoomName)
                .ToArrayAsync();

        }
        public async Task<ActionResult<string[]>> GetAllRoomName()
        {
            return await _bookingContext.Rooms
                .Select(u => u.RoomName)
                .ToArrayAsync();
        }

        [HttpPost]
        public async Task<ActionResult<Room>> CreateRoom([FromBody] Room room)
        {
            Room roomInDb = await _bookingContext
                .Rooms
                .FirstOrDefaultAsync(u => u.RoomName == room.RoomName);

            if (roomInDb != null)
                return Conflict("Room with this name already exists");

            _bookingContext.Rooms.Add(room);
            await _bookingContext.SaveChangesAsync();

            return Ok(room);
        }
    }
    
}